package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.repository.ConnectionSupplier;
import ru.geekfactory.homefinance.dao.repository.CurrencyRepository;


public class CurrentServiceImpl implements CurrencyService {

    @Override
    public CurrencyModel findByName(String name) {
        CurrencyRepository currencyRepository = new CurrencyRepository(new ConnectionSupplier());

        return currencyRepository.findByName(name);
    }

    @Override
    public CurrencyModel save(CurrencyModel model) {
        CurrencyRepository currencyRepository = new CurrencyRepository(new ConnectionSupplier());

        return currencyRepository.save(model);
    }
}