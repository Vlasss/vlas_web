package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.*;
import ru.geekfactory.homefinance.dao.repository.*;

import java.math.BigDecimal;

public class TransactionService {
    private TransactionRepository transactionRepository;
    private AccountRepository accountRepository;

    public TransactionService(TransactionRepository transactionRepository, AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }

    public TransactionService() {
        transactionRepository = new TransactionRepository(new ConnectionSupplier());
        accountRepository = new AccountRepository(new ConnectionSupplier());
    }

    public TransactionModel findById(Long id) {
        return transactionRepository.findByID(id);
    }

    public TransactionModel expenseOfFunds(TransactionModel model) {
        AccountUsersModel accountUsersModel = model.getAccountUsersModel();
        if (model.getProfitOrLoss()) {
            accountUsersModel.setAmount(getting(accountUsersModel.getAmount(),model.getAmount()));
        } else {
            accountUsersModel.setAmount(spending(accountUsersModel.getAmount(),model.getAmount()));
        }
        accountRepository.update(accountUsersModel);
        model.setAccountUsersModel(accountUsersModel);
        transactionRepository.save(model);
        return model;
    }

    private BigDecimal spending(BigDecimal accountUserAmount, BigDecimal transactionAmount) {
        return accountUserAmount.subtract(transactionAmount);
    }
    private BigDecimal getting(BigDecimal accountUserAmount, BigDecimal transactionAmount) {
        return accountUserAmount.add(transactionAmount);
    }

    public void deleteTransaction(Long id) {
        TransactionRepository transactionRepository = new TransactionRepository(new ConnectionSupplier());
        transactionRepository.remove(id);
    }
}