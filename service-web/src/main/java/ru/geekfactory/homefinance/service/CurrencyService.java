package ru.geekfactory.homefinance.service;


import ru.geekfactory.homefinance.dao.model.CurrencyModel;

public interface CurrencyService {

    CurrencyModel findByName(String name);

    CurrencyModel save(CurrencyModel model);

}
