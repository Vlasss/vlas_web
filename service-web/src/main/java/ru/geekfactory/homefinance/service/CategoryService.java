package ru.geekfactory.homefinance.service;

import ru.geekfactory.homefinance.dao.model.*;
import ru.geekfactory.homefinance.dao.repository.*;

public class CategoryService {
    public CategoryModel saveNewCategory(CategoryModel categoryModel){
        CategoryRepository categoryRepository = new CategoryRepository(new ConnectionSupplier());
        return categoryRepository.save(categoryModel);
    }
}
