package mockTest;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ru.geekfactory.homefinance.dao.model.*;
import ru.geekfactory.homefinance.dao.repository.*;
import ru.geekfactory.homefinance.service.*;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MockExample {

    @InjectMocks
    TransactionService transactionService;
    TransactionRepository transactionRepository = Mockito.mock(TransactionRepository.class);
    AccountRepository accountRepository = Mockito.mock(AccountRepository.class);


    @Test
    void testTransactionServiceClass() {
        when(transactionRepository.save(any())).thenReturn(null);
        doNothing().when(accountRepository).update(any());

        TransactionModel transactionModel = fillingTransactionModel();

        BigDecimal expected = transactionModel.getAccountUsersModel().getAmount().subtract(new BigDecimal("100.00"));
        BigDecimal actual = transactionService.expenseOfFunds(transactionModel).getAccountUsersModel().getAmount();

        Assert.assertEquals(expected, actual);
    }

    private TransactionModel fillingTransactionModel (){

    AccountRepository accountRepository = new AccountRepository(new ConnectionSupplier());
    CategoryRepository categoryRepository = new CategoryRepository(new ConnectionSupplier());
    TransactionTypeRepository transactionTypeRepository = new TransactionTypeRepository(new ConnectionSupplier());
        BigDecimal bigDecimal = new BigDecimal("100.00");
        Set<CategoryModel> categoryModels = new HashSet<>();
        CategoryModel categoryModel1 = categoryRepository.findByID(1);
        CategoryModel categoryModel2 = categoryRepository.findByID(2);
        categoryModel1.setTransactionModels(null);
        categoryModel2.setTransactionModels(null);
        categoryModels.add(categoryModel1);
        categoryModels.add(categoryModel2);
        return new TransactionModel("Перевод Тестовый", bigDecimal,
                false, transactionTypeRepository.findByID(1),
                accountRepository.findByID(1),
                categoryModels);
    }
}