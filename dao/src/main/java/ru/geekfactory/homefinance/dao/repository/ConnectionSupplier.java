package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.*;

import java.sql.*;

public class ConnectionSupplier {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/new_java_web_vlas?useSSL=false&serverTimezone=UTC";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "13371337q";

    public Connection connection() {
        try {
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            conn.setAutoCommit(false);

            return conn;
        } catch (SQLException e) {
            throw  new HomeFinanceException("Error in ConnectionSupplier", e);
        }
    }
}