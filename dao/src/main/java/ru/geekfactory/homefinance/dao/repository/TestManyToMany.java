package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.model.*;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Deprecated
public class TestManyToMany {
    private ConnectionSupplier connectionSupplier;

    public TestManyToMany(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }
    public void testTransactionRepositorySave() {
        AccountRepository accountRepository = new AccountRepository(connectionSupplier);
        CategoryRepository categoryRepository = new CategoryRepository(connectionSupplier);
        TransactionRepository transactionRepository = new TransactionRepository(connectionSupplier);
        TransactionTypeRepository transactionType = new TransactionTypeRepository(connectionSupplier);
        BigDecimal bigDecimal = new BigDecimal(444444);
        Set<CategoryModel> categoryModels = new HashSet<>();
        categoryModels.add(categoryRepository.findByID(1));
        categoryModels.add(categoryRepository.findByID(2));
        TransactionModel transactionModel = new TransactionModel("Перевод для Антона", bigDecimal, transactionType.findByID(1), accountRepository.findByID(1), categoryModels);
        transactionRepository.save(transactionModel);
    }

    public void testTransactionRepositoryUpdate() {
        AccountRepository accountRepository = new AccountRepository(connectionSupplier);
        CategoryRepository categoryRepository = new CategoryRepository(connectionSupplier);
        TransactionRepository transactionRepository = new TransactionRepository(connectionSupplier);
        TransactionTypeRepository transactionType = new TransactionTypeRepository(connectionSupplier);
        BigDecimal bigDecimal = new BigDecimal(1111);
        Set<CategoryModel> categoryModels = new HashSet<>();
        categoryModels.add(categoryRepository.findByID(1));
        categoryModels.add(categoryRepository.findByID(4));
        TransactionModel transactionModel = new TransactionModel("данные по адрессу айди 34",
                bigDecimal, transactionType.findByID(2),
                accountRepository.findByID(2), categoryModels);
        transactionModel.setId(34l);
        System.out.println(transactionModel);
        transactionRepository.update(transactionModel);
    }

    public void testTransactionRepositoryFindById() {
        TransactionRepository transactionRepository = new TransactionRepository(new ConnectionSupplier());
        TransactionModel transactionModel = transactionRepository.findByID(35);
        System.out.println("Вывожу данные по адресу id = 35");
        System.out.println(transactionModel);
    }

    public void testTransactionRepositoryRemove() {
        TransactionRepository transactionRepository = new TransactionRepository(new ConnectionSupplier());
        System.out.println("я удаляю данные по адресу id= 27");
        transactionRepository.remove(27);
    }


    public void testCategoryRepositorySave() {
        TransactionRepository transactionRepository = new TransactionRepository(new ConnectionSupplier());
        CategoryRepository categoryRepository = new CategoryRepository(new ConnectionSupplier());
        Set<TransactionModel> transactionModels = new HashSet<>();
        transactionModels.add(transactionRepository.findByID(1));
        transactionModels.add(transactionRepository.findByID(2));

        CategoryModel categoryModel = new CategoryModel("Тест", 1, transactionModels);
        categoryRepository.save(categoryModel);
    }

    public void testCategoryRepositoryUpdate() {
        TransactionRepository transactionRepository = new TransactionRepository(new ConnectionSupplier());
        CategoryRepository categoryRepository = new CategoryRepository(new ConnectionSupplier());
        Set<TransactionModel> transactionModels = new HashSet<>();
        transactionModels.add(transactionRepository.findByID(2));
        transactionModels.add(transactionRepository.findByID(3));

        CategoryModel categoryModel = new CategoryModel(14,"Тест, transactionModel_id = 2,3, parent id = 2", 2, transactionModels);
        categoryRepository.update(categoryModel);
    }

    public void testCategoryRepositoryFindById() {
        CategoryRepository categoryRepository = new CategoryRepository(new ConnectionSupplier());
        CategoryModel categoryModel = categoryRepository.findByID(6);
        System.out.println("Вывожу данные по адресу id= 6");
        System.out.println(categoryModel);
    }

    public void testCategoryRepositoryRemove() {
        CategoryRepository categoryRepository = new CategoryRepository(new ConnectionSupplier());
        System.out.println("я удаляю данные по адресу id= 14");
        categoryRepository.remove(14);
    }


}
