package ru.geekfactory.homefinance.dao.model;

import java.util.Objects;
import java.util.Set;

public class CategoryModel {
    private long id;
    private String nameCategory;
    private long parent_id;  //todo переделать имя
    private Set<TransactionModel> transactionModels;   //ManyToMany

    public CategoryModel() {
    }

    public CategoryModel(String nameCategory, long parent_id) {
        this.nameCategory = nameCategory;
        this.parent_id = parent_id;
    }

    public CategoryModel(String nameCategory, long parent_id, Set<TransactionModel> transactionModels) {
        this.nameCategory = nameCategory;
        this.parent_id = parent_id;
        this.transactionModels = transactionModels;
    }

    public CategoryModel(long id, String nameCategory, long parent_id, Set<TransactionModel> transactionModels) {
        this.id = id;
        this.nameCategory = nameCategory;
        this.parent_id = parent_id;
        this.transactionModels = transactionModels;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public long getParent_id() {
        return parent_id;
    }

    public void setParent_id(long parent_id) {
        this.parent_id = parent_id;
    }

    public Set<TransactionModel> getTransactionModels() {
        return transactionModels;
    }

    public void setTransactionModels(Set<TransactionModel> transactionModels) {
        this.transactionModels = transactionModels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryModel that = (CategoryModel) o;
        return id == that.id &&
                parent_id == that.parent_id &&
                Objects.equals(nameCategory, that.nameCategory) &&
                Objects.equals(transactionModels, that.transactionModels);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nameCategory, parent_id, transactionModels);
    }

    @Override
    public String toString() {
        return "CategoryModel{" +
                "id=" + id +
                ", nameCategory='" + nameCategory + '\'' +
                ", parent_id=" + parent_id +
                ", transactionModels=" + transactionModels +
                '}';
    }
}
