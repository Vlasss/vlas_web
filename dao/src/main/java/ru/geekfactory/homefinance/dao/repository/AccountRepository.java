package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.model.*;
import ru.geekfactory.homefinance.dao.*;

import java.sql.*;
import java.util.ArrayList;

public class AccountRepository implements Repository<AccountUsersModel> {
    private ConnectionSupplier connectionSupplier;
    private CurrencyRepository currencyRepository;

    public AccountRepository(ConnectionSupplier connectionSupplier) {

        this.connectionSupplier = connectionSupplier;
        this.currencyRepository = new CurrencyRepository(connectionSupplier);
    }

    @Override
    public AccountUsersModel findByID(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM  accountmodel_tbl " + "WHERE id = " + "'" + id + "'")) {

                AccountUsersModel accountUsersModel = new AccountUsersModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();                                                       // todo проверка на наличие элемента(возврат Optional)
                accountUsersModel.setId(resultSet.getInt(1));
                accountUsersModel.setName(resultSet.getString(2));
                accountUsersModel.setAmount(resultSet.getBigDecimal(3));
                accountUsersModel.setCurrencyModel(currencyRepository.findByID(resultSet.getLong(4)));
                return accountUsersModel;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in FindById AccountRepository", e);       // ингл
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in FindById AccountRepository", e);
        }
    }

    @Override
    public ArrayList<AccountUsersModel> findAll() {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM accountmodel_tbl")) {

                ArrayList<AccountUsersModel> accountUsersModel = new ArrayList<>();
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    AccountUsersModel accountUsersModels = new AccountUsersModel();
                    accountUsersModels.setId(resultSet.getInt(1));
                    accountUsersModels.setName(resultSet.getString(2));
                    accountUsersModels.setAmount(resultSet.getBigDecimal(3));
                    accountUsersModels.setCurrencyModel(currencyRepository.findByID(resultSet.getLong(4)));
                    accountUsersModel.add(accountUsersModels);
                }
                return accountUsersModel;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findAll AccountRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findAll AccountRepository", e);
        }
    }

    @Override
    public void remove(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM accountmodel_tbl  WHERE id = " + "'" + id + "'")) {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in remove AccountRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in remove AccountRepository", e);
        }
    }

    @Override
    public AccountUsersModel save(AccountUsersModel model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO accountmodel_tbl (name, amount, currency_id) VALUES (?,?,?)",
                    Statement.RETURN_GENERATED_KEYS
            )) {

                preparedStatement.setString(1, model.getName());
                preparedStatement.setBigDecimal(2, model.getAmount());
                preparedStatement.setLong(3, model.getCurrencyModel().getId());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                preparedStatement.executeUpdate();
                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in save AccountRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save AccountRepository", e);
        }
    }

    @Override
    public void update(AccountUsersModel model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE accountmodel_tbl SET name = " + "'" + model.getName() + "'" +
                    ", amount = " + "'" + model.getAmount() + "'" +
                    ", currency_id = " + "'" + model.getCurrencyModel().getId() + "'" +
                    "WHERE ID = " + model.getId())) {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Ошибка в update AccountRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Ошибка в update AccountRepository", e);
        }
    }
}