package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.*;
import ru.geekfactory.homefinance.dao.repository.*;
import ru.geekfactory.homefinance.dao.model.*;

import java.sql.*;
import java.util.ArrayList;

public class TransactionTypeRepository implements Repository<TransactionType> {
    private ConnectionSupplier connectionSupplier;

    public TransactionTypeRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    @Override
    public TransactionType findByID(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM  transaction_type_tbl " + "WHERE id = " + "'" + id + "'")) {

                TransactionType transactionType = new TransactionType();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                transactionType.setId(resultSet.getInt(1));
                transactionType.setName(resultSet.getString(2));
                return transactionType;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findByID TransactionType", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID TransactionType", e);
        }
    }

    @Override
    public ArrayList<TransactionType> findAll() {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM transaction_type_tbl")) {

                ArrayList<TransactionType> transactionTypes = new ArrayList<>();
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    TransactionType transactionType = new TransactionType();
                    transactionType.setId(resultSet.getInt(1));
                    transactionType.setName(resultSet.getString(2));
                    transactionTypes.add(transactionType);
                }
                return transactionTypes;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findAll TransactionType", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findAll TransactionType", e);
        }
    }

    @Override
    public void remove(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transaction_type_tbl WHERE id = " + "'" + id + "'")) {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in remove TransactionType", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in remove TransactionType", e);
        }
    }

    @Override
    public TransactionType save(TransactionType model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transaction_type_tbl (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS)) {

                preparedStatement.setString(1, model.getName());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }
                connection.commit();
                return model;
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in save TransactionType", e);
            }

        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save TransactionType", e);
        }
    }

    @Override
    public void update(TransactionType model) {
        try (Connection connection = connectionSupplier.connection()){
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE transaction_type_tbl SET name = " + "'" + model.getName() + "'" +
                    "WHERE ID = " + model.getId()))  {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e){
                connection.rollback();
                throw new HomeFinanceException("Error in update TransactionType", e);
            }
        }catch (SQLException e){
            throw new HomeFinanceException("Error in update TransactionType", e);
        }
    }
}
