package ru.geekfactory.homefinance.dao.model;

import java.math.BigDecimal;
import java.util.Objects;

public class AccountUsersModel {
    private long id;
    private String name;
    private BigDecimal amount;
    private CurrencyModel currencyModel;

    public AccountUsersModel() {
    }

    public AccountUsersModel(String name, BigDecimal amount, CurrencyModel currencyModel) {
        this.name = name;
        this.amount = amount;
        this.currencyModel = currencyModel;
    }

    public AccountUsersModel(long id, String name, BigDecimal amount, CurrencyModel currencyModel) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.currencyModel = currencyModel;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public CurrencyModel getCurrencyModel() {
        return currencyModel;
    }

    public void setCurrencyModel(CurrencyModel currencyModel) {
        this.currencyModel = currencyModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountUsersModel that = (AccountUsersModel) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(currencyModel, that.currencyModel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, amount, currencyModel);
    }

    @Override
    public String toString() {
        return "AccountUsersModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", currencyModel=" + currencyModel +
                '}';
    }
}
