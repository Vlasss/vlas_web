package ru.geekfactory.homefinance.dao.model;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

public class TransactionModel {
    private Long id;                // todo переделать в обертки
    private String name;
    private BigDecimal amount;
    private Boolean profitOrLoss;
    private TransactionType transactionType;
    private AccountUsersModel accountUsersModel;
    private Set<CategoryModel> categoryModels;  //ManyToMany

    public TransactionModel() {
    }

    public TransactionModel(String name, BigDecimal amount, boolean profitOrLoss, TransactionType transactionType,
                            AccountUsersModel accountUsersModel, Set<CategoryModel> categoryModels) {
        this.name = name;
        this.amount = amount;
        this.profitOrLoss = profitOrLoss;
        this.transactionType = transactionType;
        this.accountUsersModel = accountUsersModel;
        this.categoryModels = categoryModels;
    }

    public TransactionModel(Long id, String name, BigDecimal amount, boolean profitOrLoss,
                            TransactionType transactionType, AccountUsersModel accountUsersModel,
                            Set<CategoryModel> categoryModels) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.profitOrLoss = profitOrLoss;
        this.transactionType = transactionType;
        this.accountUsersModel = accountUsersModel;
        this.categoryModels = categoryModels;
    }

    public TransactionModel(String name, BigDecimal amount, TransactionType transactionType,
                            AccountUsersModel accountUsersModel, Set<CategoryModel> categoryModels) {
        this.name = name;
        this.amount = amount;
        this.transactionType = transactionType;
        this.accountUsersModel = accountUsersModel;
        this.categoryModels = categoryModels;
    }

    public TransactionModel(Long id, String name, BigDecimal amount, TransactionType transactionType,
                            AccountUsersModel accountUsersModel, Set<CategoryModel> categoryModels) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.transactionType = transactionType;
        this.accountUsersModel = accountUsersModel;
        this.categoryModels = categoryModels;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public boolean isProfitOrLoss() {
        return profitOrLoss;
    }

    public void setProfitOrLoss(boolean profitOrLoss) {
        this.profitOrLoss = profitOrLoss;
    }

    public Boolean getProfitOrLoss() {
        return profitOrLoss;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public AccountUsersModel getAccountUsersModel() {
        return accountUsersModel;
    }

    public void setAccountUsersModel(AccountUsersModel accountUsersModel) {
        this.accountUsersModel = accountUsersModel;
    }

    public Set<CategoryModel> getCategoryModels() {
        return categoryModels;
    }

    public void setCategoryModels(Set<CategoryModel> categoryModels) {
        this.categoryModels = categoryModels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionModel that = (TransactionModel) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(profitOrLoss, that.profitOrLoss) &&
                Objects.equals(transactionType, that.transactionType) &&
                Objects.equals(accountUsersModel, that.accountUsersModel) &&
                Objects.equals(categoryModels, that.categoryModels);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, amount, profitOrLoss, transactionType, accountUsersModel, categoryModels);
    }

    @Override
    public String toString() {
        return "TransactionModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", profitOrLoss=" + profitOrLoss +
                ", transactionType=" + transactionType +
                ", accountUsersModel=" + accountUsersModel +
                ", categoryModels=" + categoryModels +
                '}';
    }
}
