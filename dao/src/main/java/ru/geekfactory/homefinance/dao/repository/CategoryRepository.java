package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.*;
import ru.geekfactory.homefinance.dao.repository.*;
import ru.geekfactory.homefinance.dao.model.*;

import java.sql.*;
import java.util.*;

public class CategoryRepository implements Repository<CategoryModel> {
    private ConnectionSupplier connectionSupplier;
    private CategoryTransactionRepository categoryTransactionRepository;

    public CategoryRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
        this.categoryTransactionRepository = new CategoryTransactionRepository(connectionSupplier);
    }


    @Override
    public CategoryModel findByID(long id) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM  categorymodel_tbl " + "WHERE id = " + "'" + id + "'")) {

                CategoryModel categoryModel = new CategoryModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                categoryModel.setId(resultSet.getLong(1));
                categoryModel.setNameCategory(resultSet.getString(2));
                categoryModel.setParent_id(resultSet.getInt(3));   //todo сделать репозиторий под Parent_id
                categoryModel.setTransactionModels(categoryTransactionRepository.findByID(resultSet.getLong(1)));
                return categoryModel;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findById CategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findById CategoryRepository", e);
        }
    }

    @Override
    public List<CategoryModel> findAll() {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM categorymodel_tbl")) {

                ArrayList<CategoryModel> categoryModels = new ArrayList<>();
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    CategoryModel categoryModel = new CategoryModel();
                    categoryModel.setId(resultSet.getInt(1));
                    categoryModel.setNameCategory(resultSet.getString(2));
                    categoryModel.setParent_id(resultSet.getInt(3));
                    categoryModel.setTransactionModels(categoryTransactionRepository
                                                      .findByID(resultSet.getLong(1)));
                    categoryModels.add(categoryModel);
                }
                return categoryModels;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findAll CategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findAll CategoryRepository", e);
        }
    }

    @Override
    public void remove(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            categoryTransactionRepository.remove(id);
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM categorymodel_tbl  WHERE id = " + "'" + id + "'")) {

                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in remove CategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in remove CategoryRepository", e);
        }
    }

    @Override
    public CategoryModel save(CategoryModel model) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO categorymodel_tbl (name, parent_id) VALUES (?,?)"
                          , Statement.RETURN_GENERATED_KEYS)) {

                preparedStatement.setString(1, model.getNameCategory());
                preparedStatement.setLong(2, model.getParent_id());
                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    model.setId(resultSet.getLong(1));
                }

                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in save CategoryRepository", e);
            }

        } catch (SQLException e) {
            throw new HomeFinanceException("Error in save CategoryRepository", e);
        }
        Iterator<TransactionModel> it = model.getTransactionModels().iterator();
        while (it.hasNext()) {
            categoryTransactionRepository.saveTransaction(it.next().getId(), model.getId());
        }
        return model;
    }

    @Override
    public void update(CategoryModel model) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE categorymodel_tbl SET name = " + "'" + model.getNameCategory() + "'" +
                    ", parent_id = " + "'" + model.getParent_id() + "'" +
                    "WHERE ID = " + model.getId())) {
                categoryTransactionRepository.remove(model.getId());
                Iterator<TransactionModel> it = model.getTransactionModels().iterator();
                while (it.hasNext()) {
                    categoryTransactionRepository.save(model.getId(), it.next().getId());
                }
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in update CategoryRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in update CategoryRepository", e);
        }
    }
}