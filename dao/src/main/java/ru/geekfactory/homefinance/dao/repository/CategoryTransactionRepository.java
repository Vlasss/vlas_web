package ru.geekfactory.homefinance.dao.repository;

import ru.geekfactory.homefinance.dao.*;
import ru.geekfactory.homefinance.dao.repository.*;
import ru.geekfactory.homefinance.dao.model.*;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class CategoryTransactionRepository {
    private ConnectionSupplier connectionSupplier;
    private AccountRepository accountRepository;
    private TransactionTypeRepository transactionTypeRepository;

    public CategoryTransactionRepository(ConnectionSupplier connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
        this.accountRepository = new AccountRepository(connectionSupplier);
        this.transactionTypeRepository = new TransactionTypeRepository(connectionSupplier);
    }

    public Set<TransactionModel> findByID(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM  transaction_category_tbl " + "WHERE category_id = " + "'" + id + "'")) {

                Set<TransactionModel> transactionModels = new HashSet<>();
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    transactionModels.add(findByIdTransactionModel(resultSet.getLong(1)));
                }
                return transactionModels;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findByID CategoryTransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByID CategoryTransactionRepository", e);
        }
    }

    private TransactionModel findByIdTransactionModel(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM  transactionmodel_tbl " + "WHERE id = " + "'" + id + "'")) {

                TransactionModel transactionModel = new TransactionModel();
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                transactionModel.setId(resultSet.getLong(1));
                transactionModel.setName(resultSet.getString(2));
                transactionModel.setAmount(resultSet.getBigDecimal(3));
                transactionModel.setProfitOrLoss(resultSet.getBoolean(4));
                transactionModel.setTransactionType(transactionTypeRepository.
                                                    findByID(resultSet.getLong(5)));

                transactionModel.setAccountUsersModel(accountRepository.findByID(resultSet.getLong(6)));

                return transactionModel;
            } catch (SQLException e) {
                throw new HomeFinanceException("Error in findByIdTransactionModel CategoryTransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in findByIdTransactionModel CategoryTransactionRepository", e);
        }
    }

    public void remove(long id) {

        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM transaction_category_tbl WHERE category_id = " + "'" + id + "'")) {
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in remove CategoryTransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in remove CategoryTransactionRepository", e);
        }
    }

    public void save(long category_id, long transaction_id) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transaction_category_tbl (transaction_id, category_id) VALUES (?,?)",
                          Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setLong(1, transaction_id);
                preparedStatement.setLong(2, category_id);
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new RuntimeException("Error in save TransactionCategoryRepository", e);
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error in save TransactionCategoryRepository", e);
        }
    }

    public void saveTransaction(long transaction_id,long category_id) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("INSERT INTO transaction_category_tbl (transaction_id, category_id) VALUES (?,?)",
                            Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.setLong(1, transaction_id);
                preparedStatement.setLong(2, category_id);
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new RuntimeException("Error in save TransactionCategoryRepository", e);
            }

        } catch (SQLException e) {
            throw new RuntimeException("Error in save TransactionCategoryRepository", e);
        }
    }


    public void update(long transaction_id, long category_id) {
        try (Connection connection = connectionSupplier.connection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement
                    ("UPDATE transaction_category_tbl SET category_id = " + "'" + category_id + "'" +
                          "WHERE transaction_id = " + transaction_id)) {
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceException("Error in update CategoryTransactionRepository", e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceException("Error in update CategoryTransactionRepository", e);
        }
    }
}