package repositoryTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.geekfactory.homefinance.dao.HomeFinanceException;
import ru.geekfactory.homefinance.dao.model.CategoryModel;
import ru.geekfactory.homefinance.dao.model.TransactionModel;
import ru.geekfactory.homefinance.dao.repository.*;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;


public class TransactionRepositoryTest {
    private AccountRepository accountRepository;
    private CategoryRepository categoryRepository;
    private TransactionRepository transactionRepository;
    private TransactionTypeRepository transactionTypeRepository;

    @Before
    public void setUp() {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        accountRepository = new AccountRepository(connectionSupplier);
        categoryRepository = new CategoryRepository(connectionSupplier);
        transactionRepository = new TransactionRepository(connectionSupplier);
        transactionTypeRepository = new TransactionTypeRepository(connectionSupplier);
    }

    private TransactionModel fillingTransactionModel (){
        BigDecimal bigDecimal = new BigDecimal("100.00");
        Set<CategoryModel> categoryModels = new HashSet<>();
        CategoryModel categoryModel1 = categoryRepository.findByID(1);
        CategoryModel categoryModel2 = categoryRepository.findByID(2);
        categoryModel1.setTransactionModels(null);
        categoryModel2.setTransactionModels(null);
        categoryModels.add(categoryModel1);
        categoryModels.add(categoryModel2);
        TransactionModel transactionModel = new TransactionModel("Тестовый перевод", bigDecimal,
                false, transactionTypeRepository.findByID(1), accountRepository.findByID(1), categoryModels);

        return transactionModel;
    }

    @Test
    public void testTransactionRepositorySave() {
        TransactionModel expected = fillingTransactionModel();
        expected.setId(transactionRepository.save(expected).getId());
        TransactionModel actual = transactionRepository.findByName(expected.getName());

        Assert.assertEquals(expected, actual);

        transactionRepository.remove(actual.getId());
    }

    @Test
    public void testTransactionRepositoryUpdate() {
        TransactionModel expected =fillingTransactionModel();
        expected.setId(transactionRepository.save(expected).getId());
        expected.setName("Корм для уточек");
        transactionRepository.update(expected);
        TransactionModel actual = transactionRepository.findByID(expected.getId());

        Assert.assertEquals(expected, actual);

        transactionRepository.remove(expected.getId());
    }

    @Test(expected = HomeFinanceException.class)
    public void testTransactionRepositoryRemove() {
        TransactionModel expected = fillingTransactionModel();
        Long idNeedToRemove = transactionRepository.save(expected).getId();

        transactionRepository.remove(idNeedToRemove);
        transactionRepository.findByID(idNeedToRemove);
    }
}