package repositoryTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.geekfactory.homefinance.dao.HomeFinanceException;
import ru.geekfactory.homefinance.dao.model.CurrencyModel;
import ru.geekfactory.homefinance.dao.repository.*;

public class CurrencyRepositoryTest {
    private CurrencyRepository currencyRepository;

    @Before
    public void setUp(){
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        currencyRepository = new CurrencyRepository(connectionSupplier);
    }

    @Test
    public void testCurrencyRepositoryFindByIdAndSave() {
        CurrencyModel currencyModel = new CurrencyModel("Тестовая Валюта", "Её знак", "Символ");
        currencyModel = currencyRepository.save(currencyModel);

        Assert.assertEquals(currencyModel.getId(),
                currencyRepository.findByID(currencyModel.getId()).getId());
        Assert.assertEquals(currencyModel.getName(),
                currencyRepository.findByID(currencyModel.getId()).getName());
        Assert.assertEquals(currencyModel.getCode(),
                currencyRepository.findByID(currencyModel.getId()).getCode());
        Assert.assertEquals(currencyModel.getSymbol(),
                currencyRepository.findByID(currencyModel.getId()).getSymbol());

        currencyRepository.remove(currencyModel.getId());
    }

    @Test
    public void testCurrencyRepositoryUpdate() {
        CurrencyModel currencyModel = new CurrencyModel("Тестовая Валюта", "Её знак", "Символ");
        currencyModel = currencyRepository.save(currencyModel);
        currencyModel.setName("Измененное имя");
        currencyRepository.update(currencyModel);

        Assert.assertEquals(currencyModel.getId(),
                currencyRepository.findByID(currencyModel.getId()).getId());
        Assert.assertEquals("Измененное имя",
                currencyRepository.findByID(currencyModel.getId()).getName());
        Assert.assertEquals(currencyModel.getCode(),
                currencyRepository.findByID(currencyModel.getId()).getCode());
        Assert.assertEquals(currencyModel.getSymbol(),
                currencyRepository.findByID(currencyModel.getId()).getSymbol());

        currencyRepository.remove(currencyModel.getId());
    }


    @Test(expected = HomeFinanceException.class)
    public void testCurrencyRepositoryRemove() {
        CurrencyModel currencyModel = new CurrencyModel("Тестовая Валюта", "Её знак", "Символ");
        currencyModel = currencyRepository.save(currencyModel);

        currencyRepository.remove(currencyModel.getId());
        currencyRepository.findByID(currencyModel.getId());
    }
}