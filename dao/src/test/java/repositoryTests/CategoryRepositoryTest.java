package repositoryTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.geekfactory.homefinance.dao.HomeFinanceException;
import ru.geekfactory.homefinance.dao.model.CategoryModel;
import ru.geekfactory.homefinance.dao.model.TransactionModel;
import ru.geekfactory.homefinance.dao.repository.*;

import java.util.HashSet;
import java.util.Set;

public class CategoryRepositoryTest {
    private CategoryRepository categoryRepository;
    private TransactionRepository transactionRepository;

    @Before
    public void setUp() {
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        categoryRepository = new CategoryRepository(connectionSupplier);
        transactionRepository = new TransactionRepository(connectionSupplier);
    }

    private CategoryModel fillingTransactionModel (){
        Set<TransactionModel> transactionModels = new HashSet<>();
        TransactionModel transactionModel1 = transactionRepository.findByID(1);
        TransactionModel transactionModel2 = transactionRepository.findByID(2);
        transactionModel1.setCategoryModels(null);
        transactionModel2.setCategoryModels(null);
        transactionModels.add(transactionModel1);
        transactionModels.add(transactionModel2);
        return new CategoryModel("Тест", 1, transactionModels);
    }

    @Test
    public void testCategoryRepositoryFindByIdAndSave() {
        CategoryModel expected = fillingTransactionModel();

        expected.setId(categoryRepository.save(expected).getId());
        CategoryModel actual = categoryRepository.findByID(expected.getId());

        Assert.assertEquals(expected,actual);

        categoryRepository.remove(actual.getId());
    }

    @Test
    public void testCategoryRepositoryUpdate() {
        CategoryModel expected = fillingTransactionModel();
        expected = categoryRepository.save(expected);
        expected.setNameCategory("Мы поменяли на корм для животных");
        categoryRepository.update(expected);

        Assert.assertEquals("Мы поменяли на корм для животных", categoryRepository.findByID
                                                                        (expected.getId()).getNameCategory());
        categoryRepository.remove(expected.getId());
    }

    @Test(expected = HomeFinanceException.class)
    public void testCategoryRepositoryRemove() {
        CategoryModel expected = fillingTransactionModel();
        expected = categoryRepository.save(expected);

        categoryRepository.remove(expected.getId());
        categoryRepository.findByID(expected.getId());
    }
}