package repositoryTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.geekfactory.homefinance.dao.*;
import ru.geekfactory.homefinance.dao.model.AccountUsersModel;
import ru.geekfactory.homefinance.dao.repository.*;


import java.math.BigDecimal;

public class AccountRepositoryTest {
    private AccountRepository accountRepository;
    private CurrencyRepository currencyRepository;

    @Before
    public void setUp(){
        ConnectionSupplier connectionSupplier = new ConnectionSupplier();
        accountRepository = new AccountRepository(connectionSupplier);
        currencyRepository = new CurrencyRepository(connectionSupplier);
    }
    @Test
    public void testAccountRepositoryFindByIdAndSave() {
        BigDecimal amount = new BigDecimal(50000);
        AccountUsersModel accountUsersModel = new AccountUsersModel("ТестовыйАндрей",amount,currencyRepository.findByID(1));
        accountUsersModel = accountRepository.save(accountUsersModel);

        Assert.assertEquals(accountUsersModel.getId(),
                            accountRepository.findByID(accountUsersModel.getId()).getId());
        Assert.assertEquals(accountUsersModel.getAmount(),
                            accountRepository.findByID(accountUsersModel.getId()).getAmount());
        Assert.assertEquals(accountUsersModel.getCurrencyModel().getId(),
                            accountRepository.findByID(accountUsersModel.getId()).getCurrencyModel().getId());

        accountRepository.remove(accountUsersModel.getId());
    }

    @Test
    public void testAccountRepositoryUpdate() {
        BigDecimal amount = new BigDecimal(50000);
        AccountUsersModel accountUsersModel = new AccountUsersModel("ТестовыйАндрей",amount,currencyRepository.findByID(1));
        accountRepository.save(accountUsersModel);
        BigDecimal newAmount = new BigDecimal(60000);
        accountUsersModel.setAmount(newAmount);
        accountRepository.update(accountUsersModel);

        Assert.assertEquals(accountUsersModel.getId(),
                accountRepository.findByID(accountUsersModel.getId()).getId());
        Assert.assertEquals(newAmount,
                accountRepository.findByID(accountUsersModel.getId()).getAmount());
        Assert.assertEquals(accountUsersModel.getCurrencyModel().getId(),
                accountRepository.findByID(accountUsersModel.getId()).getCurrencyModel().getId());

        accountRepository.remove(accountUsersModel.getId());
    }



    @Test(expected = HomeFinanceException.class)
    public void testAccountRepositoryRemove() {
        BigDecimal amount = new BigDecimal(50000);
        AccountUsersModel accountUsersModel = new AccountUsersModel("Тестовый Андрей",amount,currencyRepository.findByID(1));
        accountUsersModel = accountRepository.save(accountUsersModel);

        accountRepository.remove(accountUsersModel.getId());
        accountRepository.findByID(accountUsersModel.getId());
    }
}
