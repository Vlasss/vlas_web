CREATE TABLE currencyModel_tbl
(
    id     INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name   VARCHAR(50)                    NOT NULL,
    code   VARCHAR(50)                    NOT NULL,
    symbol VARCHAR(50)                    NOT NULL
);

CREATE TABLE parent_category_tbl
(
    id   INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(50)                    NOT NULL
);

CREATE TABLE accountModel_tbl
(
    id          INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name        VARCHAR(50)                    NOT NULL,
    amount      decimal(15, 0)                 NOT NULL,
    currency_id INT                            NOT NULL,

    CONSTRAINT accountModel_currencyModel_fk FOREIGN KEY (currency_id) REFERENCES currencyModel_tbl (Id)
);

CREATE TABLE categoryModel_tbl
(
    id        INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name      VARCHAR(50)                    NOT NULL,
    parent_id INT                            NOT NULL,

    CONSTRAINT categoryModel_tbl_parent_category_fk FOREIGN KEY (parent_id) REFERENCES parent_category_tbl (Id)
);

CREATE TABLE transaction_type_tbl
(
    id   INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(50)                    NOT NULL
);

CREATE TABLE transactionModel_tbl
(
    id           INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name         VARCHAR(50)                    NOT NULL,
    amount       decimal(15, 2)                 NOT NULL,
    profitOrLoss BOOLEAN                        NOT NULL,
    type_id      INT                            NOT NULL,
    account_id   INT                            NOT NULL,

    CONSTRAINT transactionModel_transaction_type_fk FOREIGN KEY (type_id) REFERENCES transaction_type_tbl (Id),
    CONSTRAINT transactionModel_accountModel_fk FOREIGN KEY (account_id) REFERENCES accountModel_tbl (Id)
);

CREATE TABLE transaction_category_tbl
(
    transaction_id INT NOT NULL,
    category_id    INT NOT NULL,

    CONSTRAINT transaction_category_transactionModel_fk FOREIGN KEY (transaction_id) REFERENCES transactionModel_tbl (Id),
    CONSTRAINT transaction_category_categoryModel_fk FOREIGN KEY (category_id) REFERENCES categoryModel_tbl (Id)
);



INSERT INTO currencyModel_tbl (name, code, symbol)
VALUES ('Доллар', '1', '$');
INSERT INTO currencyModel_tbl (name, code, symbol)
VALUES ('Рубль', '2', '₽');
INSERT INTO currencyModel_tbl (name, code, symbol)
VALUES ('Евро', '3', '€');

INSERT INTO accountmodel_tbl (NAME, AMOUNT, CURRENCY_ID)
VALUES ('Andre', '200000', 1);
INSERT INTO accountmodel_tbl (NAME, AMOUNT, CURRENCY_ID)
VALUES ('Bill', '300000', 2);
INSERT INTO accountmodel_tbl (NAME, AMOUNT, CURRENCY_ID)
VALUES ('Durex', '400000', 3);

INSERT INTO parent_category_tbl (name)
VALUES ('Товары для дома');
INSERT INTO parent_category_tbl (name)
VALUES ('Траты на а/м');
INSERT INTO parent_category_tbl (name)
VALUES ('Перемещение средств-переводы');


INSERT INTO categoryModel_tbl (name, parent_id)
VALUES ('Продукты', 1);
INSERT INTO categoryModel_tbl (name, parent_id)
VALUES ('Моющее средства', 1);
INSERT INTO categoryModel_tbl (name, parent_id)
VALUES ('Запчасти для а/м', 2);
INSERT INTO categoryModel_tbl (name, parent_id)
VALUES ('Мойка а/м', 2);
INSERT INTO categoryModel_tbl (name, parent_id)
VALUES ('Переводы', 3);



INSERT INTO transaction_type_tbl(name)
VALUES ('Перевод');
INSERT INTO transaction_type_tbl(name)
VALUES ('Поступление средств');
INSERT INTO transaction_type_tbl(name)
VALUES ('Зарплата');
INSERT INTO transaction_type_tbl(name)
VALUES ('Траты');

INSERT INTO transactionModel_tbl(name, amount, profitOrLoss, type_id, account_id)
VALUES ('Перевод Саньку с соседнего падъезда', '10000', FALSE, 1, 1);
INSERT INTO transactionModel_tbl(name, amount, profitOrLoss, type_id, account_id)
VALUES ('покупка молока', '100', FALSE, 4, 2);
INSERT INTO transactionModel_tbl(name, amount, profitOrLoss, type_id, account_id)
VALUES ('зарплата', '50000', TRUE, 4, 3);

INSERT INTO transaction_category_tbl (transaction_id, category_id)
VALUES (1, 2);

#
# SELECT *
# FROM transactionmodel_tbl
# WHERE account_id = 1;
#
#
# DELETE
# FROM transactionmodel_tbl
# WHERE id = 4;
# DELETE
# FROM transactionmodel_tbl
# WHERE id = 7;
# DELETE
# FROM transactionmodel_tbl
# WHERE id = 8;
# DELETE
# FROM transactionmodel_tbl
# WHERE id = 9;
#
# UPDATE transactionmodel_tbl
# SET name       = 'Йена2',
#     amount     = 5000,
#     type_id    = 2,
#     account_id = '1'
# where id = 30;